
package co.com.proyectobase.screenplay.runners;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
		//features="src/test/resources/features/TraductorGoogle.feature", tags= "@traducir",
		//features="src/test/resources/features/Reto1.feature", tags= "@registry",glue="co.com.proyectobase.screenplay.stepdefinitions",
		features="src/test/resources/features/Maxtime.feature", tags= "@Maxtime1",glue="co.com.proyectobase.screenplay.stepdefinitions",
		snippets=SnippetType.CAMELCASE		)
public class RunnerTags {

}
