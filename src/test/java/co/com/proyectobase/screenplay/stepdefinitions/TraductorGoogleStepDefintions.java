package co.com.proyectobase.screenplay.stepdefinitions;


import static org.hamcrest.Matchers.equalTo;

import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.questions.LaRespuesta;
import co.com.proyectobase.screenplay.tasks.Abrir;
import co.com.proyectobase.screenplay.tasks.Traducir;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class TraductorGoogleStepDefintions {
	
	@Managed(driver="chrome")
	private WebDriver hisBrowser;
	private Actor Jorge = Actor.named("Jorge");
	
	@Before
	public void configuracionInicial() {
		Jorge.can(BrowseTheWeb.with(hisBrowser));
		}
	
	
	@Given("^que Jorge quiere usar le traductor de Google$")
	public void queJorgeQuiereUsarLeTraductorDeGoogle() throws Exception {
	  Jorge.wasAbleTo(Abrir.LaPaginaDeGoogle()); 
	  
	}

	@When("^el traduce la palabra (.*) de ingles a español$")
	public void elTraduceLaPalabraTableDeInglesAEspañol(String palabra)  {
		Jorge.attemptsTo(Traducir.DeInglesAEspañol(palabra));
	    
	}

	@Then("^el deberia ver la palabra (.*) en la pantalla$")
	public void elDeberiaVerLaPalabraMesaEnLaPantalla(String palabraEsperada)  {
		Jorge.should(seeThat(LaRespuesta.es(),equalTo(palabraEsperada)));
		
		
	  
	}


	

}
