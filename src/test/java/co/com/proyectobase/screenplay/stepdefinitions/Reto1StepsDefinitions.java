package co.com.proyectobase.screenplay.stepdefinitions;

import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.tasks.Ingresar;
import co.com.proyectobase.screenplay.tasks.Registra;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class Reto1StepsDefinitions {
	
	@Managed(driver="chrome")
	private WebDriver hisBrowser;
	private Actor Carlos = Actor.named("Carlos");
	
	@Before
	public void configuracionInicial() {
		Carlos.can(BrowseTheWeb.with(hisBrowser));
		}
	
	@Given("^that Carlos wants to access the Web Automation Demo Site$")
	public void thatCarlosWantsToAccessTheWebAutomationDemoSite() throws Exception {
		Carlos.wasAbleTo(Ingresar.ALaPaginaWebAutomationDemoSite());
	   
	}

	@When("^he performs the registration on the page$")
	public void hePerformsTheRegistrationOnThePage() throws Exception {
		Carlos.attemptsTo(Registra.SuUsuario());
	   
	}

	@Then("^he verifies that the screen is loaded with text Double Click$")
	public void heVerifiesThatTheScreenIsLoadedWithTextDoubleClick() throws Exception {
	  
	}
	

}
