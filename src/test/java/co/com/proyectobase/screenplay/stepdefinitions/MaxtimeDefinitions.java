package co.com.proyectobase.screenplay.stepdefinitions;

import java.util.List;

import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.model.RegistroMaxtime;
import co.com.proyectobase.screenplay.tasks.Entrar;
import co.com.proyectobase.screenplay.tasks.Register;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class MaxtimeDefinitions {
	@Managed(driver="chrome")
	private WebDriver hisBrowser;
	private Actor jorge = Actor.named("Jorge");
	
	@Before
	public void configuracionInicial() {
		jorge.can(BrowseTheWeb.with(hisBrowser));
		}
	
	@Given("^that Jorge wants to access the Web Maxtime$")
	public void thatJorgeWantsToAccessTheWebMaxtime() throws Exception {
		jorge.wasAbleTo(Entrar.ALaPaginaMaxtime());
	  
	}

	@When("^he records his daily activities$")
	public void heRecordsHisDailyActivities(DataTable registroMaxtime) {
		jorge.attemptsTo(Register.YourDailyActivities(new RegistroMaxtime(registroMaxtime)));
	   
	}

	@Then("^I validate the outcomes$")
	public void iValidateTheOutcomes() throws Exception {
	 
	}



}
