package co.com.proyectobase.screenplay.model;

import cucumber.api.DataTable;

public class RegistroMaxtime {
	private String nom_usuario;
	private String contraseña;
	private String pmo;
	private String tipo_hora;
	private String tipo_servicio;
	private String actividad;
	private String horas;
	private String comentario;
	
	
	
	public RegistroMaxtime(DataTable registroMaxtime) {
		this.nom_usuario = registroMaxtime.asList(String.class).get(0);
		this.contraseña = registroMaxtime.asList(String.class).get(1);
		this.pmo = registroMaxtime.asList(String.class).get(2);
		this.tipo_servicio = registroMaxtime.asList(String.class).get(3);
		this.actividad = registroMaxtime.asList(String.class).get(4);
		this.horas = registroMaxtime.asList(String.class).get(5);
		this.comentario = registroMaxtime.asList(String.class).get(6);
	}
	public String getNom_usuario() {
		return nom_usuario;
	}
	
	public String getContraseña() {
		return contraseña;
	}
	
	public String getPmo() {
		return pmo;
	}
	
	public String getTipo_hora() {
		return tipo_hora;
	}
	
	public String getTipo_servicio() {
		return tipo_servicio;
	}
	
	public String getActividad() {
		return actividad;
	}
	
	public String getHoras() {
		return horas;
	}
	
	public String getComentario() {
		return comentario;
	}
	
	
	
	

}
