package co.com.proyectobase.screenplay.tasks;

import co.com.proyectobase.screenplay.ui.WebAutomationPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Open;

public class Ingresar implements Task {
	
	private WebAutomationPage webAutomationPage;

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Open.browserOn(webAutomationPage));
		
		
	}

	public static Ingresar ALaPaginaWebAutomationDemoSite() {
		
		return Tasks.instrumented(Ingresar.class);
	}

}
