package co.com.proyectobase.screenplay.tasks;

import co.com.proyectobase.screenplay.ui.MaxtimeHomePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Open;

public class Entrar implements Task {
	private MaxtimeHomePage maxtimeHomePage;
		

	@Override
	public <T extends Actor> void performAs(T actor) {
	actor.attemptsTo(Open.browserOn(maxtimeHomePage));
		
		
	}

	public static Entrar ALaPaginaMaxtime() {
		return Tasks.instrumented(Entrar.class);
	}

}
