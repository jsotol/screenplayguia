package co.com.proyectobase.screenplay.tasks;

import org.openqa.selenium.remote.server.handler.SendKeys;

import co.com.proyectobase.screenplay.ui.WebAutomationPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class Registra implements Task {
	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Click.on(WebAutomationPage.CAMPO_FIRST_NAME));
		actor.attemptsTo(Enter.theValue("jorge").into(WebAutomationPage.CAMPO_FIRST_NAME));
		actor.attemptsTo(Enter.theValue("soto").into(WebAutomationPage.CAMPO_LAST_NAME));
		actor.attemptsTo(Enter.theValue("Cll 5a").into(WebAutomationPage.CAMPO_ADRESS));
		actor.attemptsTo(Enter.theValue("jsoto@hotmail.com").into(WebAutomationPage.CAMPO_IMAIL));
		actor.attemptsTo(Enter.theValue("3117827660").into(WebAutomationPage.CAMPO_PHONE));
		actor.attemptsTo(Click.on(WebAutomationPage.CAMPO_GENDER));
		actor.attemptsTo(Click.on(WebAutomationPage.BOTON_HOBBIES));
		//actor.attemptsTo(Click.on(WebAutomationPage.CAMPO_LENGUAJE));
		//actor.attemptsTo(Click.on(WebAutomationPage.CAMPO_TIPO_LENGUAJE));
		//actor.attemptsTo(Click.on(WebAutomationPage.CAMPO_SKILLS));
		actor.attemptsTo(Click.on(WebAutomationPage.CAMPO_COUNTRY));
		actor.attemptsTo(Click.on(WebAutomationPage.CAMPO_COUNTRYSELECT));
		actor.attemptsTo(Click.on(WebAutomationPage.CAMPO_YAER));
		actor.attemptsTo(Click.on(WebAutomationPage.CAMPO_YEARSELECT));
		actor.attemptsTo(Click.on(WebAutomationPage.CAMPO_MONTH));
		actor.attemptsTo(Click.on(WebAutomationPage.CAMPO_SELECTMONTH));
		actor.attemptsTo(Click.on(WebAutomationPage.CAMPO_DAY));
		actor.attemptsTo(Click.on(WebAutomationPage.CAMPO_SELECTDAY));
		actor.attemptsTo(Enter.theValue("Jsoto0102*").into(WebAutomationPage.CAMPO_PSSW));
		actor.attemptsTo(Enter.theValue("Jsoto0102*").into(WebAutomationPage.CAMPO_PSSW2));
		
		
		
		
		
		
			
		
	}
	
public static Registra SuUsuario() {
		return Tasks.instrumented(Registra.class);
	}

	

}
