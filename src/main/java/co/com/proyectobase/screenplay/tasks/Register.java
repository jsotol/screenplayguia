package co.com.proyectobase.screenplay.tasks;

import java.util.List;

import co.com.proyectobase.screenplay.model.RegistroMaxtime;
import co.com.proyectobase.screenplay.ui.MaxtimeHomePage;
import co.com.proyectobase.screenplay.ui.WebAutomationPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class Register implements Task {
	
	private RegistroMaxtime registroMaxtime;
	
	
	
	public Register(RegistroMaxtime registroMaxtime) {
		this.registroMaxtime = registroMaxtime;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Click.on(MaxtimeHomePage.USER));
		actor.attemptsTo(Enter.theValue(registroMaxtime.getNom_usuario()).into(MaxtimeHomePage.USER));
		actor.attemptsTo(Enter.theValue(registroMaxtime.getContraseña()).into(MaxtimeHomePage.PASS));
		actor.attemptsTo(Click.on(MaxtimeHomePage.CONECTION));
		actor.attemptsTo(Click.on(MaxtimeHomePage.CHKDATE));
		actor.attemptsTo(Click.on(MaxtimeHomePage.DATE));
		//actor.attemptsTo(Click.on(MaxtimeHomePage.NEW));
		
	}

	public static Register YourDailyActivities(RegistroMaxtime registroMaxtime) {
		return Tasks.instrumented(Register.class,registroMaxtime);
	}

	

}
