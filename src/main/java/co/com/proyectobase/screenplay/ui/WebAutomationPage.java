package co.com.proyectobase.screenplay.ui;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://demo.automationtesting.in/Register.html")
public class WebAutomationPage extends PageObject {
	public static final Target CAMPO_FIRST_NAME = Target.the("Campo para diligenciar el nombre").locatedBy("//input[contains(@ng-model,'irstName')]");
	public static final Target CAMPO_LAST_NAME = Target.the("Campo para diligenciar el apellido").locatedBy("//input[contains(@ng-model,'astName')]");
	public static final Target CAMPO_ADRESS = Target.the("Campo para diligenciar el direccion").locatedBy("//*[@id=\'basicBootstrapForm\']/div[2]/div/textarea");
	public static final Target CAMPO_IMAIL = Target.the("Campo para diligenciar imail").locatedBy("//input[contains(@ng-model,'Adress')]");
	public static final Target CAMPO_PHONE = Target.the("Campo para diligenciar tel").locatedBy("//input[contains(@ng-model,'Phone')]");
	public static final Target CAMPO_GENDER = Target.the("Boton para seleccionar sexo").locatedBy("//input[contains(@ng-model,'radiovalue')]");
	public static final Target BOTON_HOBBIES = Target.the("Boton para seleccionar hobbies ").located(By.id("checkbox2"));
	public static final Target CAMPO_LENGUAJE = Target.the("Campo para seleccionar lenguaje ").located(By.id("msdd"));
	//public static final Target CAMPO_TIPO_LENGUAJE = Target.the("Campo para seleccionar tipo lenguaje ").locatedBy("//input[contains(@class,'Arabic')]");
	//public static final Target CAMPO_SKILLS = Target.the("Campo para seleccionar skills ").located(By.id("Skills"));
	public static final Target CAMPO_COUNTRY = Target.the("Campo para seleccionar ciudad ").located(By.id("countries"));
	public static final Target CAMPO_COUNTRYSELECT = Target.the("Campo para seleccionar listaciudad ").locatedBy("//*[@id=\"countries\"]/option[2]");
	public static final Target CAMPO_YAER = Target.the("Campo para seleccionar Año").locatedBy("//*[@id=\"yearbox\"]");
	public static final Target CAMPO_YEARSELECT = Target.the("Campo para seleccionar listaaño ").locatedBy("//*[@id=\"yearbox\"]/option[80]");
	public static final Target CAMPO_MONTH = Target.the("Campo para seleccionar mes ").locatedBy("//*[@id=\"basicBootstrapForm\"]/div[11]/div[2]/select");
	public static final Target CAMPO_SELECTMONTH = Target.the("Campo para seleccionar listmes ").locatedBy("//*[@id=\"basicBootstrapForm\"]/div[11]/div[2]/select/option[2]");
	public static final Target CAMPO_DAY = Target.the("Campo para seleccionar dia ").located(By.id("daybox"));
	public static final Target CAMPO_SELECTDAY = Target.the("Campo para seleccionar listdia ").locatedBy("//*[@id=\"daybox\"]/option[2]");
	public static final Target CAMPO_PSSW = Target.the("Campo digitar Password ").located(By.id("Password"));
	public static final Target CAMPO_PSSW2 = Target.the("Campo digitar Password2 ").located(By.id("secondpassword"));
}
