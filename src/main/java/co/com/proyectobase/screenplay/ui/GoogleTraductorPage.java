package co.com.proyectobase.screenplay.ui;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class GoogleTraductorPage {
	public static final Target BOTON_LENGUAJE_ORIGEN = Target.the("El boton idioma origen").located(By.id("gt-sl-gms"));
	public static final Target BOTON_LENGUAJE_DESTINO = Target.the("El boton idioma DESTINO").located(By.id("gt-tl-gms"));
	public static final Target OPCION_INGLES = Target.the("la opcion ingles").located(By.xpath("//div[@id='gt-sl-gms-menu']/table/tbody//tr/td//div[contains(text(),'ingl')]"));
	public static final Target OPCION_ESPAÑOL = Target.the("la opcion español").located(By.xpath("//div[@id='gt-tl-gms-menu']/table/tbody//tr/td//div[contains(text(),'espa')]"));
	public static final Target AREA_DE_TRADUCCION = Target.the("El lugar donde se escriben las palabras a traducir").located(By.id("source"));
	public static final Target BOTON_TRADUCIR = Target.the("Boton traducir").located(By.id("gt-submit"));
	public static final Target AREA_TRADUCIDA = Target.the("Area de respuesta").located(By.id("gt-res-dir-ctr"));
	
}   


