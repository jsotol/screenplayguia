package co.com.proyectobase.screenplay.ui;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://www.choucairtesting.com:18000/MaxtimeCHC/Login.aspx?ReturnUrl=%2fMaxTimeCHC%2f")
public class MaxtimeHomePage extends PageObject {
	public static final Target USER = Target.the("the user is diligent").located(By.id("Logon_v0_MainLayoutEdit_xaf_l30_xaf_dviUserName_Edit_I"));
	public static final Target PASS = Target.the("The password is diligent").located(By.id("Logon_v0_MainLayoutEdit_xaf_l35_xaf_dviPassword_Edit_I"));
	public static final Target CONECTION = Target.the("Conecting Button").located(By.id("Logon_PopupActions_Menu_DXI0_T"));
	public static final Target CHKDATE = Target.the("CHKDATE").located(By.id("Vertical_v1_LE_v2_header5_SCh_S_D"));
	//public static final Target DATE = Target.the("date field").located(By.xpath("//*[starts-with(@id,'Vertical_v1_LE_v2_cell0_0_xaf_Fecha') and contains(@id, 'Vertical_v1_LE_v2_cell0_0_xaf_Fecha_View')]"));
	public static final Target DATE = Target.the("date field").located(By.id("Vertical_v1_LE_v2_tccell0_0"));
	//public static final Target NEW = Target.the("new button").located(By.linkText("Nuevo"));
	
}
